﻿using Microsoft.Owin;
using Owin;
using Configuration;
using IdentityServer3.Core.Configuration;
using Serilog;
using IdentityServer3.Host.Config;
using Microsoft.Owin.Security.OpenIdConnect;

[assembly: OwinStartup(typeof(WebHost.Startup))]

namespace WebHost
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Trace(outputTemplate: "{Timestamp} [{Level}] ({Name}){NewLine} {Message}{NewLine}{Exception}")
                .CreateLogger();

            var factory = new IdentityServerServiceFactory()
                        .UseInMemoryUsers(Users.Get())
                        .UseInMemoryClients(Clients.Get())
                        .UseInMemoryScopes(Scopes.Get());

            var options = new IdentityServerOptions
            {
                SigningCertificate = Certificate.Load(),
                Factory = factory,
                AuthenticationOptions = new AuthenticationOptions
                {
                    IdentityProviders = ConfigureAdditionalIdentityProviders,
                }
            };

            app.Map("", idsrvApp =>
            {
                idsrvApp.UseIdentityServer(options);
            });
        }

        public static void ConfigureAdditionalIdentityProviders(IAppBuilder app, string signInAsType)
        {
            var openid = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "MyId",
                Caption = "My ID",
                ClientId = "mvc.owin.hybrid",
                ClientSecret = "secret",
                Authority = "https://fhirauth.azurewebsites.net/",

                RedirectUri = "https://fhirauthorization.azurewebsites.net/",

                ResponseType = "code id_token",
                Scope = "openid profile",

                SignInAsAuthenticationType = signInAsType,
            };
            app.UseOpenIdConnectAuthentication(openid);

            var id = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "MyId",
                Caption = "ID-døren",
                ClientId = "ehelse_test",
                ClientSecret = "passord",
                Authority = "https://eid-exttest.difi.no/opensso/oauth2",
                
                RedirectUri = "https://localhost:44300/",

                ResponseType = "code",
                Scope = "openid",
                
                SignInAsAuthenticationType = signInAsType,
            };
            app.UseOpenIdConnectAuthentication(id);

        }
        }
}